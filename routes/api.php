<?php

use App\Http\Api\ExampleModel\ExampleModelController;
use Illuminate\Support\Facades\Route;

/** SuppliedData */
Route::prefix('exampleModelVendor')->group(function () {
    Route::get('exampleModels/{exampleModelId}', [ExampleModelController::class, 'getOne'])
        ->name('exampleModelVendor.getOne');

    Route::get('exampleModels', [ExampleModelController::class, 'getList'])
        ->name('exampleModelVendor.getList');

    Route::post('exampleModels', [ExampleModelController::class, 'add'])
        ->name('exampleModelVendor.add');

    Route::put('exampleModels/{exampleModelId}', [ExampleModelController::class, 'update'])
        ->name('exampleModelVendor.update');

    Route::delete('exampleModels/{exampleModelId}', [ExampleModelController::class, 'delete'])
        ->name('exampleModelVendor.delete');
});

Route::prefix('exampleModels')->group(function () {
    Route::get('{exampleModelId}', [ExampleModelController::class, 'getOne'])
        ->name('exampleModel.getOne');

    Route::get('', [ExampleModelController::class, 'getList'])
        ->name('exampleModel.getList');

    Route::post('', [ExampleModelController::class, 'add'])
        ->name('exampleModel.add');

    Route::put('{exampleModelId}', [ExampleModelController::class, 'update'])
        ->name('exampleModel.update');

    Route::delete('{exampleModelId}', [ExampleModelController::class, 'delete'])
        ->name('exampleModel.delete');
});
