<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \App\Contracts\DataStorage\ExampleModelContract as Storage;

return new class extends Migration
{
    /**
     * @return void
     */
    public function up(): void
    {
        if (!Schema::hasTable(Storage::TABLE_NAME)) {
            Schema::create(Storage::TABLE_NAME, function (Blueprint $table) {
                $table->increments(Storage::PRIMARY_KEY);

                $table->string(Storage::NAME, 63)->comment('Name');
                $table->string(Storage::EMAIL, 63)->nullable()->comment('E-Mail');
                $table->timestamp(Storage::EMAIL_VERIFIED_AT)->nullable()->comment('E-Mail verified at');
                $table->string(Storage::PASSWORD, 1023)->nullable()->comment('Password');
                $table->string(Storage::REMEMBER_TOKEN, 1023)->nullable()->comment('Remember token');
                $table->string(Storage::ADDRESS, 255)->nullable()->comment('Address');
                $table->string(Storage::PHONE, 63)->nullable()->comment('Phone');
                $table->string(Storage::MISCELLANEOUS, 63)->nullable()->comment('Miscellaneous');
            });
        }
    }

    /**
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists(Storage::TABLE_NAME);
    }
};
