<?php

namespace Database\Seeders;

use App\Contracts\DataStorage\ExampleModelContract;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ExampleModelSeeder extends Seeder
{
    /**
     * @return void
     */
    public function run(): void
    {
        DB::table(ExampleModelContract::TABLE_NAME)->insert($this->getDecoratorProductionData());
    }

    /**
     * @return array
     */
    private function getDecoratorProductionData(): array
    {
        $data = [];

        for ($iteration = 0; $iteration < 50; $iteration++) {
            $data[] = [
                ExampleModelContract::NAME => fake()->unique()->name(),
                ExampleModelContract::EMAIL => fake()->unique()->safeEmail(),
                ExampleModelContract::EMAIL_VERIFIED_AT => now(),
                ExampleModelContract::PASSWORD => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi',
                ExampleModelContract::REMEMBER_TOKEN => Str::random(10),
                ExampleModelContract::ADDRESS => fake()->unique()->address(),
                ExampleModelContract::PHONE => fake()->unique()->phoneNumber(),
                ExampleModelContract::MISCELLANEOUS => fake()->unique()->text(63),
            ];
        }

        return $data;
    }
}
