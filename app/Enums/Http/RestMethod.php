<?php

namespace App\Enums\Http;

enum RestMethod: string implements RestMethodContract
{
    case Get = self::GET;
    case Post = self::POST;
    case Put = self::PUT;
    case Delete = self::DELETE;
}
