<?php

namespace App\Http\Controllers;

use App\Services\ExampleModelService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ExampleModelController extends Controller
{
    public function __construct(protected ExampleModelService $exampleModelService)
    {
    }

    /**
     * @param Request $request
     * @param string $exampleModelId
     * @return JsonResponse
     */
    public function getOne(Request $request, string $exampleModelId): JsonResponse
    {
        $data = $this->exampleModelService->getOne($exampleModelId);

        return new JsonResponse(data: $data, json: true);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request): JsonResponse
    {
        $data = $this->exampleModelService->getList();

        return new JsonResponse(data: $data, json: true);
    }
}
