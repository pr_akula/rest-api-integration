<?php

namespace App\Http\Api\ExampleModel;

use App\Http\Controllers\Controller;
use App\Models\ExampleModel;
use Illuminate\Http\Request;

class ExampleModelController extends Controller
{
    /**
     * @param Request $request
     * @param string $exampleModelId
     * @return mixed
     */
    public function getOne(Request $request, string $exampleModelId): mixed
    {
        return ExampleModel::where(ExampleModel::PRIMARY_KEY, $exampleModelId)->firstOrFail();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getList(Request $request): array
    {
        $exampleModels = [];

        $count = 0;
        foreach (ExampleModel::all() as $model) {
            $count++;
            $exampleModels[] = $model;
            if ($count == 10) {
                break;
            }
        };

        return $exampleModels;
    }

    public function add(Request $request)
    {
        return [];
    }

    public function update(Request $request)
    {
        return [];
    }

    public function delete(Request $request)
    {
        return [];
    }
}
