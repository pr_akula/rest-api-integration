<?php

namespace App\Models;

use App\Contracts\DataStorage\ExampleModelContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExampleModel extends Model implements ExampleModelContract
{
    use HasFactory;
}
