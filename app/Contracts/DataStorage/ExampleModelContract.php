<?php

namespace App\Contracts\DataStorage;

interface ExampleModelContract
{
    public const TABLE_NAME                         = 'example_models';

    public const PRIMARY_KEY                        = self::ID;

    public const ID                                 = 'example_model_id';
    public const NAME                               = 'name';
    public const EMAIL                              = 'email';
    public const EMAIL_VERIFIED_AT                  = 'email_verified_at';
    public const PASSWORD                           = 'password';
    public const REMEMBER_TOKEN                     = 'remember_token';
    public const ADDRESS                            = 'address';
    public const PHONE                              = 'phone';
    public const MISCELLANEOUS                      = 'miscellaneous';
}
