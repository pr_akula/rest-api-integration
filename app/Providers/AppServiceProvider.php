<?php

namespace App\Providers;

use App\Http\Controllers\ExampleModelController;
use App\Integrations\ExampleModelVendor\ExampleModelVendorConnector;
use App\Services\ExampleModelService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        //
    }

    /**
     * @return void
     */
    public function boot(): void
    {
        // bind ExampleModelService from Integrations directory as singleton ('shared' = true)
        $this->app->singleton('integrations.exampleModelService', ExampleModelVendorConnector::class);

        $this->app->when(ExampleModelController::class)
            ->needs(ExampleModelService::class)
            ->give(function () {
                return new ExampleModelService();
            });
    }
}
