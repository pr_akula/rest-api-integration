<?php

namespace App\Services;

use App\Integrations\Integration;
use Illuminate\Support\Facades\Log;
use Throwable;

class ExampleModelService extends BusinessLogicService
{
    /**
     * @param int|string $exampleModelId
     * @return mixed
     */
    public function getOne(int|string $exampleModelId): mixed
    {
        $cacheKey = self::composeCorrespondingCacheKey(__METHOD__);
        self::requestFromCache($cacheKey);

        /** Way through Model facade */
        // $data = ExampleModel::where(ExampleModelContract::PRIMARY_KEY, $exampleModelId)->get();

        /** Way through Integration facade */
        try {
            $data = Integration::choose('integrations.exampleModelVendor')
                ->useMethod('getOne')
                ->withParameters([
                    'modelId' => $exampleModelId,
                ])
                ->apply();

            self::requestToCache(cacheKey: $cacheKey, value: $data, ttl: 86400);
        } catch (Throwable $throwable) {
            $message = $throwable->getMessage(); // You can set custom message
            Log::critical($message);
            $data = self::prepareResponseDataOnFailure($throwable, $message);
        }

        return $data;
    }

    /**
     * @return mixed
     */
    public function getList(): mixed
    {
        $cacheKey = self::composeCorrespondingCacheKey(__METHOD__);
        self::requestFromCache($cacheKey);

        /** Way through Model facade */
        // $data = ExampleModel::whereBetween(ExampleModelContract::PRIMARY_KEY, [1, 10])->get();

        /** Way through Integration facade */
        try {
            $data = Integration::choose('integrations.exampleModelVendor')
                ->useMethod('getList')
                ->apply();

            self::requestToCache(cacheKey: $cacheKey, value: $data, ttl: 86400);
        } catch (Throwable $throwable) {
            $message = $throwable->getMessage(); // You can set custom message
            Log::critical($message);
            $data = self::prepareResponseDataOnFailure($throwable, $message);
        }

        return $data;
    }
}
