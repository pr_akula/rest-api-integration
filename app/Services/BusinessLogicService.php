<?php

namespace App\Services;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use RuntimeException;
use Throwable;

class BusinessLogicService
{
    /**
     * @param string $cacheKey
     * @return mixed|void
     */
    protected static function requestFromCache(string $cacheKey)
    {
        if (Cache::has($cacheKey)) {
            return Cache::get($cacheKey);
        }
    }

    /**
     * @param string $cacheKey
     * @param mixed $value
     * @param int $ttl
     * @return void
     */
    protected static function requestToCache(string $cacheKey, mixed $value, int $ttl): void
    {
        Cache::set(key: $cacheKey, value: $value, ttl: 86400);
    }

    /**
     * @param string $callPoint
     * @return string
     */
    protected static function composeCorrespondingCacheKey(string $callPoint): string
    {
        $exploded = explode('::', $callPoint);
        $classFullName = $exploded[0] ?? null;
        $classMethodName = isset($exploded[1]) ? Str::camel($exploded[1]) : null;
        $className = $classFullName === null ? null : Str::camel(Arr::last(explode('\\', $classFullName)));

        if ($className === null || $classMethodName === null) {
            throw new RuntimeException('Unable to compose cache key');
        }

        return $className . '.' . $classMethodName;
    }

    protected static function prepareResponseDataOnFailure(Throwable $throwable, string $customMessage): array
    {
        return [
            'status' => 'Failure',
            'code' => $throwable->getCode(),
            'message' => empty($customMessage) ? $throwable->getMessage() : $customMessage,
        ];
    }
}
