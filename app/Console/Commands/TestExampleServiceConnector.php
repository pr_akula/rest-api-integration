<?php

namespace App\Console\Commands;

use App\Integrations\ExampleModelVendor\ExampleModelVendorConnector;
use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;

class TestExampleServiceConnector extends Command
{
    /**
     * @var string
     */
    protected $signature = 'example-model-service:test';

    /**
     * @var string
     */
    protected $description = 'Test example model service';

    /**
     * @return int
     */
    public function handle()
    {
        $exampleModelServiceConnector = new ExampleModelVendorConnector();
        $data = $exampleModelServiceConnector->getList();

        return CommandAlias::SUCCESS;
    }
}
