<?php

namespace App\Integrations;

use Illuminate\Support\Facades\Facade;
use RuntimeException;
use Throwable;

class Integration extends Facade
{
    private ?string $name;

    private ?string $method;

    private ?array $parameters;

    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'integration';
    }

    public function choose(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    public function useMethod(string $method): static
    {
        $this->method = $method;

        return $this;
    }

    public function withParameters(array $parameters = []): static
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function apply(): mixed
    {
        if ($this->name === null) {
            throw new RuntimeException('Integration\'s name wasn\'t set.');
        }

        if ($this->method === null) {
            throw new RuntimeException('Integration\'s method wasn\'t set.');
        }

        if ($this->parameters === null) {
            throw new RuntimeException('Integration\'s parameters wasn\'t set.');
        }

        try {
            $instance = app('integrations.' . self::convenientIntegrationName($this->name));

            return $instance->{$this->method}(array_values($this->parameters));
            //call_user_func_array(array($foo, "bar"), array("three", "four"));
        } catch (Throwable $throwable) {
            return $throwable;
        }
    }

    private static function convenientIntegrationName(string $string): string
    {
        return lcfirst($string);
    }
}
