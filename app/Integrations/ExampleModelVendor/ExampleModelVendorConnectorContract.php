<?php

namespace App\Integrations\ExampleModelVendor;

interface ExampleModelVendorConnectorContract
{
    public const BASE_URL = '2v-modules.local/api/exampleModelService/exampleModels/';
}
