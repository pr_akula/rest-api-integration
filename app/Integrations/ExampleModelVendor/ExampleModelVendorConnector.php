<?php

namespace App\Integrations\ExampleModelVendor;

use App\Integrations\Enums\HttpMethod;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use Throwable;

class ExampleModelVendorConnector implements ExampleModelVendorConnectorContract
{
    protected ClientInterface $client;

    public function __construct(ClientInterface $client = new Client([]))
    {
        $this->client = $client;
    }

    /**
     * @param int|string $modelId
     * @return mixed
     */
    public function getOne(int|string $modelId): mixed
    {
        try {
            return json_decode($this->client
                ->request(HttpMethod::GET, route('exampleModelVendor.getOne', [$modelId]))
                ->getBody()
                ->getContents(), true);
        } catch (Throwable $throwable) {
            return $throwable;
        }
    }

    /**
     * @return mixed
     */
    public function getList(): mixed
    {
        try {
            return json_decode($this->client
                ->request(HttpMethod::GET, route('exampleModelVendor.getList'))
                ->getBody()
                ->getContents(), true);
        } catch (Throwable $throwable) {
            return $throwable;
        }
    }
}
