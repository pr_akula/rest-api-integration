<?php

namespace App\Integrations\Enums;

use App\Integrations\Contracts\HttpMethodContract;

enum HttpMethod: string implements HttpMethodContract
{
    case Get = self::GET;
    case Head = self::HEAD;
    case Post = self::POST;
    case Put = self::PUT;
    case Delete = self::DELETE;
    case Connect = self::CONNECT;
    case Options = self::OPTIONS;
    case Trace = self::TRACE;
    case Patch = self::PATCH;
}
